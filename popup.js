function switchCaption(button) {
  chrome.storage.sync.get(["ukrLatSwitch"], function(items) {
    if (items.ukrLatSwitch) {
      button.value = "Switch off";
    } else {
      button.value = "Switch on";
    }

    button.disabled = false;
  });
}

document.addEventListener("DOMContentLoaded", function () {
  var button = document.getElementById("btnSwitch");

  switchCaption(button);

  button.addEventListener("click", function () {
    chrome.storage.sync.get(["ukrLatSwitch"], function(items) {
      switchValue = items.ukrLatSwitch;

      if (switchValue) {
        switchValue = false;
      } else {
        switchValue = true;
      }

      button.disabled = true;
      button.value = "Saving...";

      chrome.storage.sync.set({"ukrLatSwitch": switchValue}, function () {
        switchCaption(button);
      });
    });
  });
});
