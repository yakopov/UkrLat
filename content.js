// character replacement map following https://nachasi.com/ul/manifest/
var alphabet = {
  "а": "a",
  "б": "b",
  "в": "v",
  "г": "g",
  "ґ": "ğ",
  "д": "d",
  "е": "e",
  "є": "je",
  "ж": "ž",
  "з": "z",
  "и": "y",
  "і": "i",
  "ї": "ї",
  "й": "j",
  "к": "k",
  "л": "l",
  "м": "m",
  "н": "n",
  "о": "o",
  "п": "p",
  "р": "r",
  "с": "s",
  "т": "t",
  "у": "u",
  "ф": "f",
  "х": "h",
  "ц": "c",
  "ч": "ć",
  "ш": "ś",
  "щ": "ść",
  "ь": "‘",
  "ю": "ju",
  "я": "ja"
};

// @todo:lazy loops everywhere so far, but should be replaced with functional approach

// add uppercase versions to replacement map
for (var letter in alphabet) {
    // @todo: this will convert "Щось" to "ŚĆоs‘", but only the first letter should be capitalised
    alphabet[letter.toUpperCase()] = alphabet[letter].toUpperCase();
}
// expression to search for all the letters which are keys in the map above
// taking advantage of the fact they are all single characters so it's easier
var alphabetRegexp = new RegExp("([" + Object.keys(alphabet).join("") + "])", "g");

function ukrToLat() {
  // @todo: check document meta, if any, for Ukrainian language tag
  var elements = document.getElementsByTagName("*");

  // looping through all documents elements
  for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      // looping through all element nodes...
      for (var j = 0; j < element.childNodes.length; j++) {
          var node = element.childNodes[j];

          // ...or more precisely, through text nodes
          if (node.nodeType === 3) {
              /*
              1 for an element node
              2 for an attribute node
              3 for a text node
              8 for comment
              */

              var text = node.nodeValue;
              var toLowercase = [];

              var replacedText = text.replace(
                  alphabetRegexp,
                  function(all, letter, index) {
                      var nextChar = text.charAt(index);
                      var ukrLetter = alphabet[letter];

                      // now ppercase replacements which consist of more than one character...
                      if ((ukrLetter.length > 1) && (ukrLetter.toUpperCase() == ukrLetter)) {
                        if ((nextChar == nextChar.toLowerCase()) && (nextChar in alphabet)) {
                          // ...followec by a lowercase letter should have only the first chararter uppercased
                          ukrLetter = ukrLetter.charAt(0) + ukrLetter.toLowerCase().slice(1);
                        }
                      }

                      return ukrLetter;
                  }
              );

              if (replacedText !== text) {
                  element.replaceChild(document.createTextNode(replacedText), node);
              }
          }
      }
  }
}

chrome.storage.onChanged.addListener(function(changes, namespace) {
  if ("ukrLatSwitch" in changes) {
    var storageChange = changes["ukrLatSwitch"];

    if (storageChange.oldValue != storageChange.newValue) { // is it redundant since it's onChanged? anyway.
      window.location.reload(); // reload the content with now enabled or disabled processing
    }
  }
});

chrome.storage.sync.get(["ukrLatSwitch"], function(items) {
  if (items.ukrLatSwitch) {
    ukrToLat();
  }
});
