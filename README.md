# What is this?
A simple Chrome extension which converts Ukrainian Cyrillic to Latin script
following the [Manifest ukraїnśkoї latynky](https://nachasi.com/ul/manifest).

![Ukrainian language Wikipedia page converted to Latin script](https://gitlab.com/yakopov/UkrLat/raw/7e744bb5cfdde5a7185132116e06aa7c5160197f/doc/wikipedia.png)

# What's new?
* v0.2 Added popup switch feature, corrected capitalisation of two-letter replacements
* v0.1 First version

# What's next?
* Language auto-detect
* Switching on and off without a page reload
* Indicating switch status via the button icon

# How to install?
0. This extension is not yet published in Chrome store, you will have to install it manually. It's simple:
1. Download (`Repository` / `Files` / `Download as zip` / unpack downloaded file) or clone this project
2. Go to [chrome://extensions](chrome://extensions) in your Chrome browser and enable `Developer Mode`
3. Click `Load unpacked extension` and choose the project folder
4. Click the extension icon and switch it on
4. Visit a Ukrainian (or really any Cyrillic - there is no language detection yet) website
